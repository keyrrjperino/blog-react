import os
import webapp2
import jinja2

jinja_environment = jinja2.Environment(
    loader=jinja2.FileSystemLoader(
        os.path.dirname("")
    ),
    autoescape=True
)


class Dashboard(webapp2.RequestHandler):
    def get(self, instance=None):
        template = jinja_environment.get_template(
            "application/frontend/dashboard.html"
        )
        output = template.render(
            {}
        )
        self.response.out.write(output)
