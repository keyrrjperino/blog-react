import webapp2
import json
from application.models.user import User
from application.models.blog import Blog
from application.handlers.api.base import BaseHandler
from google.appengine.ext import ndb
from google.appengine.datastore.datastore_query import Cursor


class LoginApiHandler(BaseHandler):
    def get(self):
        if self.user:
            self.api_response["data"] = [self.user.to_api_object()]

        self.render()

    def post(self):
        self.response.headers["Content-Type"] = "application/json"

        email = self.request.get("email")
        password = self.request.get("password")

        user = User.query(
            User.email == email, User.password == password
        ).get()

        if user:
            self.login(user)

            api_response = {
                "status": 200,
                "data": user.to_api_object(),
                "description": "login successfully!"
            }
        else:
            api_response = {
                "status": 400,
                "data": {},
                "description": "Failed to Login!"
            }

        self.response.out.write(json.dumps(api_response))

    def put(self):
        self.logout()

        api_response = {
            "status": 200,
            "description": "logout successfully!"
        }

        self.response.out.write(json.dumps(api_response))


class UserApiHandler(webapp2.RequestHandler):
    def get(self):
        api_response = {
            "data": [],
            "more": False,
            "cursor": None
        }

        cursor = None
        if self.request.get('cursor').strip():
            cursor = Cursor(urlsafe=self.request.get('cursor'))

        self.response.headers["Content-Type"] = "application/json"

        users, next_cursor, more = User.query(
        ).fetch_page(10, start_cursor=cursor)
        for user in users:
            api_response["data"].append(user.to_api_object())

        api_response["cursor"] = next_cursor.urlsafe() if more else None
        api_response["more"] = more

        self.response.out.write(json.dumps(api_response))

    def post(self):
        self.response.headers["Content-Type"] = "application/json"
        name = self.request.get("name")
        username = self.request.get("username")
        email = self.request.get("email")
        password = self.request.get("password")

        user = User()
        user.name = name
        user.username = username
        user.password = password
        user.email = email
        user.put()

        api_response = {
            "data": user.to_api_object(),
            "status": 201,
            "description": "Data has been created!"
        }

        self.response.out.write(json.dumps(api_response))

    def put(self):
        self.response.headers["Content-Type"] = "application/json"
        user_id = self.request.get("id")
        name = self.request.get("name")

        api_response = {
            "status": 201,
            "description": "Data has been updated!"
        }

        try:
            if user_id and name:
                user = User.get_by_id(int(user_id))
                user.name = name
                user.put()
            else:
                api_response = {
                    "status": 200,
                    "description": "missing parameter(s)"
                }

        except Exception, e:
            api_response = {
                "status": 500,
                "description": str(e)
            }

        self.response.out.write(json.dumps(api_response))

    def delete(self):
        self.response.headers["Content-Type"] = "application/json"
        user_id = self.request.get("id")

        api_response = {
            "status": 201,
            "description": "Data has been deleted!"
        }

        try:
            ndb.Key('User', int(user_id)).delete()

        except Exception, e:
            api_response = {
                "status": 500,
                "description": str(e)
            }

        self.response.out.write(json.dumps(api_response))


class BlogApiHandler(webapp2.RequestHandler):
    def get(self):
        api_response = {
            "data": [],
            "more": False,
            "cursor": None
        }

        cursor = None
        if self.request.get('cursor').strip():
            cursor = Cursor(urlsafe=self.request.get('cursor'))

        self.response.headers["Content-Type"] = "application/json"

        blogs, next_cursor, more = Blog.query(
        ).fetch_page(10, start_cursor=cursor)
        for blog in blogs:
            api_response["data"].append(blog.to_api_object())

        api_response["cursor"] = next_cursor.urlsafe() if more else None
        api_response["more"] = more

        self.response.out.write(json.dumps(api_response))

    def post(self):
        self.response.headers["Content-Type"] = "application/json"
        user_id = self.request.get("user_id")
        title = self.request.get("title")
        content = self.request.get("content")
        tags = self.request.get_all("tags")

        blog = Blog()
        blog.user_key = ndb.Key('User', int(user_id))
        blog.title = title
        blog.content = content
        blog.tags = tags
        blog.put()

        api_response = {
            "data": blog.to_api_object(),
            "status": 201,
            "description": "Data has been created!"
        }

        self.response.out.write(json.dumps(api_response))

    def delete(self):
        self.response.headers["Content-Type"] = "application/json"
        blog_id = self.request.get("id")

        api_response = {
            "status": 201,
            "description": "Data has been deleted!"
        }

        try:
            ndb.Key('Blog', int(blog_id)).delete()

        except Exception, e:
            api_response = {
                "status": 500,
                "description": str(e)
            }

        self.response.out.write(json.dumps(api_response))
