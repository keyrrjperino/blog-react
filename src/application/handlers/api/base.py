import webapp2
import json
from application.models.user import User


class BaseHandler(webapp2.RequestHandler):
    def __init__(self, request=None, response=None):
        self.initialize(request, response)
        self.session = self.get_session()
        self.user = self.get_current_user()

        self.api_response = {
            "status": 200,
            "data": [],
            "description": ""
        }

    def get_session(self):
        from application.libraries.gaesessions import get_current_session
        return get_current_session()

    def get_current_user(self):
        try:
            if "user" in self.session:
                user = User.get_by_id(self.session["user"])
                return user
            else:
                return None
        except:
            return None

    def login(self, user):
        self.session["user"] = user.key.id()

    def logout(self):
        if self.session.is_active():
            self.session.terminate()
            self.user = None

    def render(self):
        self.response.headers["Content-Type"] = "application/json"
        self.response.out.write(json.dumps(self.api_response))
