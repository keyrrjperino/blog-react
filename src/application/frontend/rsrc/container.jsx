var React = require('../../../../gulp/node_modules/react');
var Axios = require('../../../../gulp/node_modules/axios');
var BrowserHistory = require('../../../../gulp/node_modules/react-router').browserHistory;

var Header = require("./header");
var Footer = require("./footer");


module.exports = React.createClass({
	getInitialState: function() {
	    return {
	          user: {
	          }
	    };
	},
	componentDidMount: function() {
	      Axios.get("/api/v1/login").then(function(response) {
	      	this.setState({
	      		user: response.data.data[0]
	      	});
	      }.bind(this));
	},
	render: function() {
		var childrenWithProps = React.Children.map(this.props.children, function(child) {
			return React.cloneElement(child, {
				user: this.user,
				loginHandler: this.loginHandler
			});
		}.bind({
			user: this.state.user,
			loginHandler: this.loginHandler
		}));
		return (
			<div className="wrapper">
				<Header user={this.state.user} />
				{childrenWithProps}
				<Footer />
			</div>
		)
	},
	loginHandler: function() {
		var formData = new FormData();
		formData.append("email", document.getElementById("email").value);
		formData.append("password", document.getElementById("password").value);
		Axios.post("/api/v1/login", formData).then(function(response) {
			if (response.data.status === 200) {
				BrowserHistory.push("/");
			}
		});
	}
});