var React = require('../../../../gulp/node_modules/react');
var ReactDOM = require('../../../../gulp/node_modules/react-dom');

var ReactRouter = require('../../../../gulp/node_modules/react-router');
var Router = ReactRouter.Router;
var Route = ReactRouter.Route;
var browserHistory = ReactRouter.browserHistory;
var IndexRoute = ReactRouter.IndexRoute;


var Container = require("./container");
var Home = require("./home");
var Profile = require("./profile");
var Contact = require("./contact");
var Blog = require("./blog");
var Login = require("./login");

var BlogDetail = require("./blog-detail");

var NoMatch = require("./nomatch");


var MainApp = React.createClass({
	render: function() {
		return (
			<Router history={browserHistory}>
				<Route
					component={Container}
					path="/">
					<IndexRoute
						component={Home} />
					<Route
						component={Profile}
						path="profile" />
					<Route
						component={Contact}
						path="contacts" />
					<Route
						component={Blog}
						path="blogs" />
					<Route
						component={BlogDetail}
						path="blogs/:date/:title" />
					<Route
						component={Login}
						path="login" />
				</Route>
				<Route
					path="*"
					component={NoMatch} />
			</Router>
		)
	}
});

ReactDOM.render(<MainApp />, document.getElementById("main"));