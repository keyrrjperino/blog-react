var React = require('../../../../gulp/node_modules/react');
var Grid = require('../../../../gulp/node_modules/react-bootstrap').Grid;
var Row = require('../../../../gulp/node_modules/react-bootstrap').Row;
var Col = require('../../../../gulp/node_modules/react-bootstrap').Col;
var Thumbnail = require('../../../../gulp/node_modules/react-bootstrap').Thumbnail;
var Button = require('../../../../gulp/node_modules/react-bootstrap').Button;
var Link = require('../../../../gulp/node_modules/react-router').Link;



module.exports = React.createClass({
	render: function() {
		return (
			<Grid>
			    <Row>
			    <Col xs={6} md={4}>
			      <Thumbnail src="https://pbs.twimg.com/profile_images/647332452562677760/DHWj3IfB.jpg" alt="242x200">
			        <h3>Romar's</h3>
			        <p>Blahhh...Blahhh...Blahhh...</p>
			        <p>
			          <Link className="primary" to="/blogs/2014-12-01/this-is-my-blog">Visit</Link>
			        </p>
			      </Thumbnail>
			    </Col>
			    <Col xs={6} md={4}>
			      <Thumbnail src="https://media.licdn.com/mpr/mpr/shrinknp_200_200/p/5/000/2b6/284/32b3ad4.jpg" alt="242x200">
			        <h3>Kent Astudillo</h3>
			        <p>Blahh..Blahh..Blahh..</p>
			        <p>
			          <Button bsStyle="primary">Button</Button>&nbsp;
			          <Button bsStyle="default">Button</Button>
			        </p>
			      </Thumbnail>
			    </Col>
			    </Row>
			  </Grid>
		)
	}
});