var React = require('../../../../gulp/node_modules/react');
var Link = require('../../../../gulp/node_modules/react-router').Link;



module.exports = React.createClass({
	displayName: "Header",
	render: function() {
		var displayName = this.props.user.email ? this.props.user.email : null;
		return (
			<header className="header">
				<ul className="nav nav-pills">
					<li role="presentation" className=""><Link role="button" to="/">Home</Link></li>
					<li role="presentation" className=""><Link role="button" to="/profile">Profile</Link></li>
					<li role="presentation" className=""><Link role="button" to="/contacts">Contacts</Link></li>
					<li role="presentation" className=""><Link role="button" to="/blogs">Blogs</Link></li>
					{displayName || <li role="presentation" className=""><Link role="button" to="/login">Login</Link></li>}
				</ul>
			</header>
		)
	}
});