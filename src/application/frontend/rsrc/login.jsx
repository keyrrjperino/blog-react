var React = require('../../../../gulp/node_modules/react');


module.exports = React.createClass({
	render: function() {
		return (
			<section className="template" onClick={this.props.clickMe}>
				<div className="loginGroup">
					<div className="input-group">
					  	<span className="input-group-addon" id="basic-addon1">Email</span>
					  	<input type="text" id="email" className="form-control" placeholder="Username" aria-describedby="basic-addon1" />
					</div>
					<div className="input-group">
					  	<span className="input-group-addon" id="basic-addon1">Password</span>
					  	<input type="text" id="password" className="form-control" placeholder="Username" aria-describedby="basic-addon1" />
					</div>
					<div>
						 <button class="btn btn-default" type="button" onClick={this.props.loginHandler}>Login</button>
					</div>
				</div>
		    </section>
		)
	}
});