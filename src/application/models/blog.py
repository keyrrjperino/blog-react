from google.appengine.ext import ndb


class Blog(ndb.Model):
    created = ndb.DateTimeProperty(auto_now_add=True)
    updated = ndb.DateTimeProperty(auto_now=True)
    user_key = ndb.KeyProperty()
    title = ndb.StringProperty()
    content = ndb.TextProperty()
    tags = ndb.StringProperty(repeated=True)

    def to_api_object(self):
        details = {}
        details["created"] = self.created.strftime("%Y-%m-%d %I:%M:%S")
        details["updated"] = self.updated.strftime("%Y-%m-%d %I:%M:%S")
        details["id"] = self.key.id()
        details["user_key"] = self.user_key.id()
        details["user"] = self.user_key.get().to_api_object()
        details["title"] = self.title
        details["content"] = self.content
        details["tags"] = self.tags
        return details
