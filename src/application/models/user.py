from google.appengine.ext import ndb


class User(ndb.Model):
    created = ndb.DateTimeProperty(auto_now_add=True)
    updated = ndb.DateTimeProperty(auto_now=True)
    username = ndb.StringProperty()
    name = ndb.StringProperty()
    email = ndb.StringProperty()
    password = ndb.StringProperty()

    def to_api_object(self):
        details = {}
        details["created"] = self.created.strftime("%Y-%m-%d %I:%M:%S")
        details["updated"] = self.updated.strftime("%Y-%m-%d %I:%M:%S")
        details["id"] = self.key.id()
        details["username"] = self.username
        details["name"] = self.name
        details["email"] = self.email
        return details
