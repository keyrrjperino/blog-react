import webapp2
from webapp2_extras import routes
from application.handlers.dashboard import Dashboard

# api's
from application.handlers.api.v1 import UserApiHandler
from application.handlers.api.v1 import BlogApiHandler
from application.handlers.api.v1 import LoginApiHandler

app = webapp2.WSGIApplication([
    routes.DomainRoute(r'<:.*>', [
        webapp2.Route('/', Dashboard),
        # api routes
        webapp2.Route('/api/v1/login', LoginApiHandler),
        webapp2.Route('/api/v1/users', UserApiHandler),
        webapp2.Route('/api/v1/blogs', BlogApiHandler),

        webapp2.Route(r'/<:.*>', Dashboard),
    ])
])
