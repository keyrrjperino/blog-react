from application.libraries.gaesessions import SessionMiddleware

appstats_CALC_RPC_COSTS = True


def webapp_add_wsgi_middleware(app):
    from google.appengine.ext.appstats import recording
    app = recording.appstats_wsgi_middleware(app)
    app = SessionMiddleware(
        app,
        cookie_key=(
            "9GueYkdpw2FvAXcZNeqG31tPHU53kbgCL4xqjNMJmYcBMcrCsvA2Q12xOftGS4Hic1"
            "TDkpZHbQKM2DB220BhF7wCMF3BVKylHz3L0ZJJPJa9oFRpC8euMKfT0gY1PiMYi3mV"
            "i4k73lsDVLymeA4Q3JlJLaMjXnp4SaViHDmUBqVSSGuXAy1V14TWS0cjaawknqwpSw"
            "PX5a4AjkIdXGBVnXnTW4Y2C1OF3EgVnROBD7MFp4lIwM1J2J5k8gDkCsqIFP8dYMVf"
            "Up8vuscWqTecyMzks7efZl7vOCuhxg2iFdgd2YDlszLNjhLGaHvh4aYfcU7Oa4bAYO"
            "n2EKcAOdyTVrMrZk2igoATGQS2dVgTlkTUMUWRhBSK4aWqzV4Ba3YvfNIs6cWvyXfW"
            "S9FjpM8vplcQsys1"
        )
    )
    return app
